package br.com.emmanuelneri;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeQuarkusHelloWorldIT extends QuarkusHelloWorldTest {

    // Execute the same tests but in native mode.
}