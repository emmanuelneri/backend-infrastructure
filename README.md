#backend-infrastructure

## Executando projeto como JAR com Tomcat embedded

[simple-http-embedded](https://gitlab.com/emmanuelneri/backend-infrastructure/tree/master/simple-http-embedded)

Executar ``./run.sh``
````
INFO 3600 --- [           main] b.c.e.simplehttp.SimpleHttpApplication   : Starting SimpleHttpApplication on MacBook-Pro-de-Emmanuel.local with PID 3600 (/Users/emmanuelneri/Documents/code/backend-infrastructure/simple-http-embedded/target/classes started by emmanuelneri in /Users/emmanuelneri/Documents/code/backend-infrastructure/simple-http-embedded)
INFO 3600 --- [           main] b.c.e.simplehttp.SimpleHttpApplication   : No active profile set, falling back to default profiles: default
INFO 3600 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8090 (http)
INFO 3600 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
INFO 3600 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.27]
INFO 3600 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/app]    : Initializing Spring embedded WebApplicationContext
INFO 3600 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 875 ms
INFO 3600 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
INFO 3600 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8090 (http) with context path '/app'
INFO 3600 --- [           main] b.c.e.simplehttp.SimpleHttpApplication   : Started SimpleHttpApplication in 16.717 seconds (JVM running for 22.395)
````


## Executando projeto como WAR com Wildfly standalone

[simple-http-war](https://gitlab.com/emmanuelneri/backend-infrastructure/tree/master/simple-http-war)

Executar ``./run.sh``
````
INFO  [org.jboss.modules] (main) JBoss Modules version 1.9.1.Final
INFO  [org.jboss.msc] (main) JBoss MSC version 1.4.11.Final
INFO  [org.jboss.threads] (main) JBoss Threads version 2.3.3.Final
INFO  [org.jboss.as] (MSC service thread 1-3) WFLYSRV0049: WildFly Full 18.0.1.Final (WildFly Core 10.0.3.Final) starting
INFO  [org.wildfly.security] (ServerService Thread Pool -- 27) ELY00001: WildFly Elytron version 1.10.4.Final
...
INFO  [org.jboss.as.server.deployment.scanner] (MSC service thread 1-1) WFLYDS0013: Started FileSystemDeploymentService for directory /opt/jboss/wildfly/standalone/deployments
INFO  [org.jboss.as.server.deployment] (MSC service thread 1-3) WFLYSRV0027: Starting deployment of "app.war" (runtime-name: "app.war")
...
INFO  [io.undertow.servlet] (ServerService Thread Pool -- 78) Initializing Spring embedded WebApplicationContext
INFO  [org.springframework.web.context.ContextLoader] (ServerService Thread Pool -- 78) Root WebApplicationContext: initialization completed in 878 ms
INFO  [org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor] (ServerService Thread Pool -- 78) Initializing ExecutorService 'applicationTaskExecutor'
INFO  [br.com.emmanuelneri.simplehttp.SimpleHttpApplication] (ServerService Thread Pool -- 78) Started SimpleHttpApplication in 2.646 seconds (JVM running for 13.432)
INFO  [javax.enterprise.resource.webcontainer.jsf.config] (ServerService Thread Pool -- 78) Initializing Mojarra 2.3.9.SP04 for context '/app'
INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 78) WFLYUT0021: Registered web context: '/app' for server 'default-server'
INFO  [org.jboss.as.server] (ServerService Thread Pool -- 44) WFLYSRV0010: Deployed "app.war" (runtime-name : "app.war")
...
INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0025: WildFly Full 18.0.1.Final (WildFly Core 10.0.3.Final) started in 14490ms - Started 532 of 758 services (374 services are lazy, passive or on-demand)

````

## Executando projeto como WAR com microprofile na implementação Thorntail

[simple-http-microprofile](https://gitlab.com/emmanuelneri/backend-infrastructure/tree/master/simple-http-microprofile)

Executar ``mvn clean thorntail:run``

````
INFO  [org.jboss.msc] (main) JBoss MSC version 1.4.5.Final
INFO  [org.jboss.threads] (main) JBoss Threads version 2.3.2.Final
INFO  [org.jboss.as] (MSC service thread 1-2) WFLYSRV0049: Thorntail 2.5.0.Final (WildFly Core 7.0.0.Final) starting
...
INFO  [org.wildfly.swarm.runtime.deployer] (main) deploying simple-http-microprofile.war
INFO  [org.jboss.as.server.deployment] (MSC service thread 1-6) WFLYSRV0027: Starting deployment of "simple-http-microprofile.war" (runtime-name: "simple-http-microprofile.war")
INFO  [org.jboss.weld.deployer] (MSC service thread 1-2) WFLYWELD0003: Processing weld deployment simple-http-microprofile.war
...
INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 9) WFLYUT0021: Registered web context: '/' for server 'default-server'
INFO  [org.jboss.as.server] (main) WFLYSRV0010: Deployed "simple-http-microprofile.war" (runtime-name : "simple-http-microprofile.war")
INFO  [org.wildfly.swarm] (main) THORN99999: Thorntail is Ready
````

Obs: Com microprofile também pode ser executado com JAR com servidor embedded

Executar ``./run``

## Configurando um proxy de portas com Nginx

[Ngix](https://gitlab.com/emmanuelneri/backend-infrastructure/tree/master/proxy/nginx)

Executar ``./run.sh``