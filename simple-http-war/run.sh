#!/usr/bin/env bash

docker build -t wildfly-exemple .

docker run -d \
   --name wildfly \
   -p 8080:8080 \
   -p 9990:9990 \
   wildfly-exemple

docker cp target/simple-http-war-1.0.0-SNAPSHOT.war wildfly:/opt/jboss/wildfly/standalone/deployments/app.war
