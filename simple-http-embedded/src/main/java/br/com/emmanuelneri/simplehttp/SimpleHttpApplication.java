package br.com.emmanuelneri.simplehttp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SimpleHttpApplication {
	public static void main(String[] args) {
		SpringApplication.run(SimpleHttpApplication.class, args);
	}

	@GetMapping
	public String hello() {
		return "Heeello";
	}

}
