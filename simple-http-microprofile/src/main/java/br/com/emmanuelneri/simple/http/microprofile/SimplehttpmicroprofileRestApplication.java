package br.com.emmanuelneri.simple.http.microprofile;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class SimplehttpmicroprofileRestApplication extends Application {
}
