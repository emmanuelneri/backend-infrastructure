#!/usr/bin/env bash

mvn clean package

docker build -t backend/app .

docker run -d \
    --name backend \
    -p 8080:8080 \
    -p 9990:9990 \
    backend/app