#!/usr/bin/env bash

docker build -t http-proxy-nginx .

## Executar projeto web: sh /proxy/web/run.sh
## Executar projeto backebd: sh /proxy/backend/run.sh
docker run -it \
    --name nginx \
    -p 80:80 \
    --link web \
    --link backend \
    http-proxy-nginx
