#!/usr/bin/env bash

openssl req -x509 -nodes -new -sha256 -days 1024 -newkey rsa:2048 -keyout server.key -out server.pem -subj "/C=US/CN=web"
openssl x509 -outform pem -in server.pem -out server.crt

docker build -t http-proxy-httpd .

docker run -it \
    --name httpd \
    -p 80:80 \
    -p 443:443 \
    http-proxy-httpd
