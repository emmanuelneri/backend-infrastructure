#!/usr/bin/env bash

mvn clean package

docker build -t web .

docker run -d \
    --name web \
    -p 8090:8090 \
    web